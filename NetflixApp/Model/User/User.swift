//
//  User.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/19/24.
//

import Foundation

struct User: Identifiable, Codable {
    let id: String
    let fullname: String
    let email: String
    var favouriteProgramsIDS: [Int] = []
    var initials: String {
        let formmater =  PersonNameComponentsFormatter()
        if let components = formmater.personNameComponents(from: fullname) {
            formmater.style = .abbreviated
            return formmater.string(from: components)
        }
        return ""
    }
}

extension User {
    static var MOOCK_USER = User(id: NSUUID().uuidString, fullname: "Uros Rupar", email: "ruparuros@gmail.com")
}
