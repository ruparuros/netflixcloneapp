//
//  NewProgramsRequest.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/14/24.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let newProgramsRequest = try? JSONDecoder().decode(NewProgramsRequest.self, from: jsonData)

import Foundation

// MARK: - NewProgramsRequest
struct NewProgramsRequest: Codable {
    let dates: Dates
    let page: Int
    let results: [Movie]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case dates, page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - Dates
struct Dates: Codable {
    let maximum, minimum: String
}
