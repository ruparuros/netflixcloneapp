//
//  TrailerRequest.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/7/24.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let trailerRequest = try? JSONDecoder().decode(TrailerRequest.self, from: jsonData)

import Foundation

// MARK: - TrailerRequest
struct TrailerRequest: Codable {
    let id: String
    let tmdbID: Int
    let imdbID, language, title: String
    let url: String
    let trailer: Trailer
    let videos: [Trailer]

    enum CodingKeys: String, CodingKey {
        case id
        case tmdbID = "tmdb_id"
        case imdbID = "imdb_id"
        case trailer
        case language
        case title
        case url
        case videos
//        case language, title, url, trailer, videos
    }
}

// MARK: - Trailer
struct Trailer: Codable {
    let id: String
    let youtubeVideoID: String
       // let youtubeChannelID: String
//    let youtubeThumbnail: String
//    let title: String
//    let thumbnail: String
//    let language: String
//    let categories: [String]
//    let published: Date
//    let views: Int

    enum CodingKeys: String, CodingKey {
        case id
        case youtubeVideoID = "youtube_video_id"
//        case youtubeChannelID = "youtube_channel_id"
//        case youtubeThumbnail = "youtube_thumbnail"
//        case title, thumbnail, language, categories, published, views
    }
}
