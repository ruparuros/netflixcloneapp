//
//  GenresRequest.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import Foundation

// MARK: - GenresRequest
struct GenresRequest: Codable {
    let genres: [Genre]
}

// MARK: - Genre
struct Genre: Codable, Identifiable {
    let id: Int
    let name: String
}
