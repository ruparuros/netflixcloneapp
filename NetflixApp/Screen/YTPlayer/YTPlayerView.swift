//
//  YTPlayerView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/8/24.
//

import Foundation
import YouTubePlayerKit
import SwiftUI

struct YTPlayerView: View {

   @Binding var videoID: String?
    var body: some View {
        
        let youTubePlayer = YouTubePlayer(
            source: .video(id: videoID ?? ""),
            configuration: .init(
                autoPlay: true
            ))
        
        YouTubePlayerView(youTubePlayer) { state in
            switch state {
            case .idle:
                ProgressView()
            case .ready:
                EmptyView()
            case .error(let error):
                Text(verbatim: "YouTube player couldn't be loaded -\(error)")
            }
        }
        
    }

}
