//
//  MovieDetailViewModel.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/7/24.
//

import Foundation

@MainActor
class MovieDetailVM: ObservableObject {
    private let movieManager: MovieManagerProtocol
    @Published var cast: [Cast] = []
    @Published var videoID: String?
    init() {
        self.movieManager = MovieManager(networkManager: NetworkManager.shared)
    }
        
    func getCast(id: Int) {
        Task {
            do {
                cast = try await movieManager.getMovieDataAsync(id: id)
            } catch {
                print(error)
            }
        }
    }
    
    func getVideos(id: Int) async {
        do {
            videoID =  try await movieManager.getTrailerVideo(id: id).youtubeVideoID
        } catch {
            print(error)
        }
    }
}
