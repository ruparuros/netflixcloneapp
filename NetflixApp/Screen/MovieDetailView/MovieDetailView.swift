//
//  MovieDetailView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/5/24.
//

import SwiftUI
import YouTubePlayerKit
import Kingfisher

struct MovieDetailView: View {
    
    @Binding var selectedMovie: Movie?
    @StateObject var movieDetaiilVM = MovieDetailVM()
    @State var  videoID: String?
    @EnvironmentObject var authVM: AuthenticationVM
    @State var addedToList: Bool = false
    var body: some View {
        ZStack {
            Color.secondaryDetail
                .ignoresSafeArea()
            
            VStack {
                ZStack(alignment: .topTrailing) {
                    
                    if movieDetaiilVM.videoID != nil {
                        YTPlayerView(videoID: $movieDetaiilVM.videoID)
                            .frame(height: 300)
                            .zIndex(-1)
                    } else {
                        KFImage.url(Api.getImageUrl(imagePath: selectedMovie?.backdropPath ?? ""))
                            .placeholder { _ in
                                Color(.black)
                            }
                            .resizable()
                            .aspectRatio(contentMode: .fill )
                            .frame(width: UIScreen.main.bounds.size.width, height: 300)
                            .zIndex(-1)
                    }
                    
                    HStack(alignment:.top ,spacing: 10) {
                        Spacer()
                        
                        Button(action: {
                            Task {
                                Task {
                                    if addedToList {
                                        
                                        try await authVM.removeFromFavourites(id: selectedMovie!.id)
                                        addedToList = false
                                    } else {
                                        
                                        try await authVM.addToFavourites(id: selectedMovie!.id)
                                        addedToList = true
                                    }
                                    
                                }
                                
                            }
                        }, label: {
                            Image(systemName: addedToList ? "checkmark.circle.fill" : "plus")
                                .frame(width: 40, height: 40)
                                .foregroundStyle(.white)
                                .background(.gray)
                                .clipShape(Circle())
                        })
                        
                        Button(action: {
                            selectedMovie = nil
                        }, label: {
                            
                            Image(systemName: "xmark")
                                .frame(width: 40, height: 40)
                                .foregroundStyle(.white)
                                .background(.gray)
                                .clipShape(Circle())
                        })
                    }
                    .zIndex(1)
                    .padding()
                }
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .leading) {
                        Text(selectedMovie?.title ?? "")
                            .foregroundStyle(.white)
                            .fontWeight(.semibold)
                            .font(.system(size: 25))
                            .padding(.top,8)
       
                        HStack(alignment: .center) {
                            Text("\(Int.random(in: 60...100))% match")
                                .foregroundStyle(.green)
                                .fontWeight(.bold)
                                .font(.system(size: 14))
                            
                            Text(selectedMovie?.date ?? Date(), format: .dateTime.year())
                                .foregroundStyle(.white)
                                .fontWeight(.medium)
                                .font(.system(size: 14))
                        
                            Text(selectedMovie?.adult ?? false ? "18+" : "12+")
                                .fontWeight(.bold)
                                .foregroundStyle(.white)
                                .font(.system(size: 10))
                                .padding(2)
                                .background(.white.opacity(0.5))
                            
                            Text(selectedMovie?.date ?? Date(), format: .dateTime.year())
                                .foregroundStyle(.white)
                                .fontWeight(.medium)
                                .font(.system(size: 14))
                            
                            Image(systemName: "ellipsis.message")
                                .foregroundStyle(.white)
                                .opacity(0.8)
                            
                            Image(systemName: "music.note.house.fill")
                                .foregroundStyle(.white)
                                .opacity(0.8)
                        }
                        
                        VStack(spacing: 12) {
                            Button {} label: {
                                
                                Spacer()
                                Label("Play", systemImage: "play.fill")
                                    .foregroundStyle(.black)
                                    .fontWeight(.bold)
                                    .font(.system(size: 18))
                                    .padding(15)
                                
                                Spacer()
                                
                            }
                            .background(.white)
                            .cornerRadius(6)
                            
                            Button {} label: {
                                Spacer()
                                Label("Downaload", systemImage: "square.and.arrow.down")
                                    .foregroundStyle(.white
                                    )
                                    .fontWeight(.bold)
                                    .font(.system(size: 18))
                                    .padding(15)
                                
                                Spacer()
                                
                            }
                            .background(.white.opacity(0.6))
                            .cornerRadius(6)
                        }

                        VStack(alignment: .leading, spacing: 10) {
                            Text(selectedMovie?.overview ?? "")
                                .fontWeight(.medium)
                                .foregroundStyle(.white)
                                .font(.system(size: 16))
                            
                            let castString = movieDetaiilVM.cast.map {$0.name}.prefix(6).reduce("", { $0 + $1 + ", "})
                            
                            Text(castString)
                                .foregroundStyle(.white.opacity(0.8))
                                .font(.system(size: 14))
                                .lineSpacing(0.5)
                            
                            HStack(alignment: .top, spacing: 20) {
                                Spacer()
                                Button {
                                   
                                    Task {
                                        if addedToList {
                                            
                                            try await authVM.removeFromFavourites(id: selectedMovie!.id)
                                            addedToList = false
                                        } else {
                                            
                                            try await authVM.addToFavourites(id: selectedMovie!.id)
                                            addedToList = true
                                        }
                                        
                                    }
                                } label: {
                                    
                                    ReactionButtonStyleView(title: "My List", systemImageName: addedToList ? "checkmark.circle.fill" : "plus")
                                       
                                }
                    
                                Button {} label: {
                                    ReactionButtonStyleView(title: "Rate", systemImageName: "hand.thumbsup")
                                }
                                
                                Button {} label: {
                                    ReactionButtonStyleView(title: "Share", systemImageName: "paperplane")
                                }
                                Spacer()
                            }
                        }
                        .padding(.vertical,10)
                    }
                    .padding()
                }
            }
            .onAppear {
                movieDetaiilVM.getCast(id: selectedMovie?.id ?? 550)
                withAnimation {
                    addedToList = authVM.currentUser!.favouriteProgramsIDS.contains(selectedMovie!.id)
                }
            }
            .task {
                await movieDetaiilVM.getVideos(id: selectedMovie?.id ?? 299534)
            }
        }
    }
}

#Preview {
    MovieDetailView(selectedMovie: .constant( MovieMockData.exampleMovie1))
}

struct ReactionButtonStyleView: View {
    var title: String
    var systemImageName: String
    var body: some View {
        VStack(spacing: 5) {
            Image(systemName: systemImageName)
                .resizable()
                .frame(width: 20, height: 20)
                .clipped()
            Text(title)
                .font(.system(size: 15))
                .fontWeight(.bold)
            
        }
        .foregroundStyle(.white)
        .padding(.horizontal, 15)
    }
}
