//
//  FavouritesVM.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/7/24.
//

import Foundation

@MainActor
class FavouritesVM: ObservableObject {
    @Published var programs: [MovieDetail]?
    var programsManager: ProgramManagerProtocol
    
    init() {
        self.programsManager = ProgramManager(networkManager: NetworkManager.shared)
    }
    
    func getProgramsIDS(IDS: [Int]) {
        Task {
            var programs:[MovieDetail] = []
            for id in IDS {
                do {
                    let program =  try await programsManager.getProgramByID(id: id)
                    programs.append(program)
                    print(program.title)
                } catch {
                    print(error)
                }
            }
            
            self.programs = programs
        }
    }
}
