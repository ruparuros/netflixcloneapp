//
//  FavouritesView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/7/24.
//

import SwiftUI

struct FavouritesView: View {
    @EnvironmentObject var authVM: AuthenticationVM
    @StateObject var favouritesVM: FavouritesVM = FavouritesVM()
    private let adaptiveColumn = [
        GridItem(.adaptive(minimum: 150))
    ]
    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            
            VStack {
                if let programs = favouritesVM.programs {
                    ScrollView {
                        LazyVGrid(columns: adaptiveColumn, spacing: 20) {
                            ForEach(programs) { program in
                                FavouritesViewCell(program: program)
                            }
                        }
                    }
                } else {
                    LoadingView()
                }
            }
            .task {
                print(authVM.currentUser?.favouriteProgramsIDS ?? [])
                favouritesVM.getProgramsIDS(IDS: authVM.currentUser?.favouriteProgramsIDS ?? [])
            }
        }
    }
}

#Preview {
    FavouritesView()
}
