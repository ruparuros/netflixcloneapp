//
//  NewProgramsVM.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/14/24.
//

import Foundation

@MainActor
class NewProgramsVM: ObservableObject {
    @Published var programs: [Movie]?
    var movieManager: MovieManagerProtocol
    
    init() {
        self.movieManager = MovieManager(networkManager: NetworkManager.shared)
    }
    
    func getNewPrograms() {
        Task {
            do {
                self.programs = try await self.movieManager.getNewPrograms()
            }
            catch {
                print(error)
            }
        }
    }
}
