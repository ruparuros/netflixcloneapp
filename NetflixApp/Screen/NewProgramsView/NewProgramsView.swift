//
//  NewProgramsView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/13/24.
//

import SwiftUI

struct NewProgramsView: View {
    
   @StateObject var newProgramsVM = NewProgramsVM()
    var body: some View {
        
        ZStack {
            Color.black
                .ignoresSafeArea()
            
            if let programs = newProgramsVM.programs {
                List(programs) { program in
                NewListCell(movie: program)
                        .listRowBackground(Color.black)
            }
            .listStyle(.plain)
            .listSectionSeparator(.hidden)
            .padding(.horizontal)
            .scrollIndicators(.hidden)
            .background(.black)
               
            } else {
                NewProgramsShimmer()
            }
        }
        .task {
            newProgramsVM.getNewPrograms()
        }
    }
}

#Preview {
    NewProgramsView()
}
