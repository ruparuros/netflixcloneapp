//
//  AccountView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/13/24.
//

import SwiftUI

struct AccountView: View {
    @EnvironmentObject var authVM: AuthenticationVM
    @State var zoomed = false
    
    
    var body: some View {
        if let user = authVM.currentUser {
            NavigationStack {
                ZStack {
                    Color.black
                        .ignoresSafeArea()
                    
                    List {
                        Section {
                            
                            HStack(spacing: 15) {
                                if zoomed {
                                    Spacer()
                                }
                                    Text(user.initials)
                                        .font(.title)
                                        .fontWeight(.semibold)
                                        .foregroundStyle(.white)
                                        .frame(width: 72, height: 72)
                                        .background(.white.opacity(0.5))
                                        .clipShape(.circle)
                                        .scaleEffect((zoomed ? 4 : 3) / 3)
                                        .animation(.default, value: zoomed)
                                        .onTapGesture {
                                            zoomed.toggle()
                                        }

                                
                                VStack(alignment: .leading) {
                                    Text(user.fullname)
                                        .fontWeight(.semibold)
                                        .padding(.top,4)
                                    
                                    Text(user.email)
                                        .font(.footnote)
                                        .foregroundStyle(.white)
                                    
                                }
                            }
                        }
                        .foregroundStyle(.white)
                        .listRowBackground(Color(.secondaryDetail))
                        
                        Section("General") {
                            HStack {
                                SettingRowView(imageName: "gear", title: "Version", tintColor: .black)
                                
                                Spacer()
                                
                                Text("1.1.0")
                                    .font(.subheadline)
                                    .foregroundStyle(Color(.systemGray))
                            }
                        }
                        .foregroundStyle(.white)
                        .listRowBackground(Color(.secondaryDetail))
                        
                        Section("Account") {
                            
                            NavigationLink {
                                FavouritesView()
                            } label: {
                                SettingRowView(imageName: "heart.fill", title: "Favourites", tintColor: .red)
                                }
                            
                            Button {
                                
                                authVM.signOut()
                            } label: {
                                SettingRowView(imageName: "arrow.left.circle.fill", title: "Sign out", tintColor: .red)
                            }
                            
                            Button {
                                authVM.deleteAccount()
                            } label: {
                                SettingRowView(imageName: "xmark.circle.fill", title: "Delete account", tintColor: .red)
                            }
                        }
                        .foregroundStyle(.white)
                        .listRowBackground(Color(.secondaryDetail))
                    }
                    
                    .scrollContentBackground(.hidden)
                }
            }
            .alert(item: $authVM.alertItem) { alert in
                return Alert(title: alert.title, message: alert.message, primaryButton: alert.dismissButton, secondaryButton: .cancel())
            }
        } else {
            Text("Login")
        }
    }
}

#Preview {
    AccountView()
        .environmentObject(AuthenticationVM())
}
