//
//  LoginView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/19/24.
//

import SwiftUI

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    @EnvironmentObject var authVM: AuthenticationVM
    var body: some View {
        NavigationStack {
            VStack {
                Image("Logonetflix")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200,height: 100)
                    .padding(.vertical, 32)
                    
                VStack(spacing: 20) {
                    VStack(alignment: .leading) {
                        InputView(text: $email, title: "Email Address", placeholder: "name@example.com")
                            .textInputAutocapitalization(.never)
                        
                        if !email.isEmpty && !email.isValidEmail {
                            Text("email is not valid")
                                .font(.subheadline)
                                .foregroundStyle(.red)
                        }
                    }
                    
                    VStack(alignment: .leading) {
                        InputView(text: $password, title: "Password", placeholder: "Enter password",isSecuredField: true)
                        
                        if !password.isEmpty && password.count < 6 {
                            Text("password should has miniimum 6 characters")
                                .font(.subheadline)
                                .foregroundStyle(.red)
                        }
                    }
                    
                }
                .padding(.horizontal)
                .padding(.top, 12)
                
                Button {
                    authVM.handleLoginProcess(withEmail: email, password: password)
                } label: {
                    HStack {
                        Text("SIGN IN")
                            .fontWeight(.semibold)
                        Image(systemName: "arrow.right")
                    }
                    .foregroundStyle(.white)
                    .frame(width: UIScreen.main.bounds.width - 32, height: 48)
                }
                .background(Color(.systemBlue))
                .disabled(!formIsValid)
                .opacity(formIsValid ? 1.0 : 0.5)
                .cornerRadius(10)
                .padding(.top, 24)
                
                Spacer()
                NavigationLink {
                    RegistrationView()
                        .navigationBarBackButtonHidden(true)
                } label: {
                    HStack(spacing: 4) {
                        Text("Do not have account?")
                        Text("Sign up")
                            .fontWeight(.bold)
                    }
                    .font(.system(size: 14))
                }
            }
            .alert(item: $authVM.alertItem) { alert in
                Alert(title: alert.title, message: alert.message, dismissButton: alert.dismissButton)
            }
        }
    }
       
}

extension LoginView: AuthenticationFormProtocol {
    var formIsValid: Bool {
        return !email.isEmpty
        && email.isValidEmail
        && !password.isEmpty
        && password.count > 5
    }
}

#Preview {
    LoginView()
}
