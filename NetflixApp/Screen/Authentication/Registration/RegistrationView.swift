//
//  RegistrationView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/19/24.
//

import SwiftUI

struct RegistrationView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var fullName = ""
    @State private var confirmPassword = ""
    @EnvironmentObject var authVM: AuthenticationVM
    
    @Environment(\.dismiss) var dismiss
    var body: some View {
        VStack {
            Image("Logonetflix")
                .resizable()
                .scaledToFit()
                .frame(width: 200,height: 100)
                .padding(.vertical, 32)
            
            VStack(spacing: 24) {
                
                VStack(alignment: .leading) {
                    InputView(text: $fullName, title: "Full name", placeholder: "Enter your name")
                    
                    if fullName.count < 3{
                        Text("Field should contains minimum 3 letter")
                            .font(.subheadline)
                            .foregroundStyle(.red)
                    }
                }
        
                VStack(alignment: .leading) {
                    InputView(text: $email, title: "Email Address", placeholder: "name@example.com")
                        .textInputAutocapitalization(.never)
                    if !email.isEmpty && !email.isValidEmail {
                        Text("Email is not valid")
                            .font(.subheadline)
                            .foregroundStyle(.red)
                    }
                }
                
                VStack(alignment: .leading) {
                    InputView(text: $password, title: "Password", placeholder: "name@example.com",isSecuredField: true)
                    
                    if !password.isEmpty && password.count < 6 {
                        Text("Password should has miniimum 6 characters")
                            .font(.subheadline)
                            .foregroundStyle(.red)
                    }
                    
                }
                ZStack(alignment: .trailing) {
                    InputView(text: $confirmPassword, title: "Confirm Password", placeholder: "Confirm your passsword",isSecuredField: true)
                    
                    if !password.isEmpty && !confirmPassword.isEmpty {
                        if password == confirmPassword {
                            Image(systemName: "checkmark.circle.fill")
                                .imageScale(.large)
                                .fontWeight(.bold)
                                .foregroundColor(Color(.systemGreen))
                        } else {
                            Image(systemName: "xmark.circle.fill")
                                .imageScale(.large)
                                .fontWeight(.bold)
                                .foregroundColor(Color(.systemRed))
                        }
                    }
                }
               
            }
            .padding(.horizontal)
            .padding()
            
            Button {
                authVM.handleRegistration(withEmail:email, password:password, fullname: fullName)
            } label: {
                HStack {
                    Text("SIGN UP")
                        .fontWeight(.semibold)
                    Image(systemName: "arrow.right")
                }
                .foregroundStyle(.white)
                .frame(width: UIScreen.main.bounds.width - 32, height: 48)
            }
            .background(Color(.systemBlue))
            .disabled(!formIsValid)
            .opacity(formIsValid ? 1.0 : 0.5)
            .cornerRadius(10)
            .padding(.top, 24)
            
            Spacer()
            
            Button {
                dismiss()
            } label: {
                HStack(spacing: 4) {
                    Text("Already have account?")
                    Text("Sign in")
                        .fontWeight(.bold)
                }
                .font(.system(size: 14))
            }
        }
        .alert(item: $authVM.alertItem) { alert in
            Alert(title: alert.title, message: alert.message, dismissButton: alert.dismissButton)
        }
    }
}

extension RegistrationView: AuthenticationFormProtocol {
    var formIsValid: Bool {
        return !email.isEmpty
        && email.isValidEmail
        && !password.isEmpty
        && password.count > 5
        && confirmPassword == password
        && !fullName.isEmpty
    }
}

#Preview {
    RegistrationView()
}
