//
//  AuthenticationVM.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/19/24.
//

import SwiftUI
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth
import FirebaseFirestoreSwift

protocol AuthenticationFormProtocol {
    var formIsValid: Bool {get}
}

@MainActor
class AuthenticationVM: ObservableObject {
    
    var authManager: AuthenticationManagerProtocol
    
    @Published var userSession: Firebase.User?
    @Published var currentUser: User?
    @Published var alertItem: AlertItem?
    
    init() {
        authManager = AuthenticationManager()
        self.userSession = authManager.getCurrentUser()
        Task {
            do {
                currentUser =  try await authManager.fetchUser()
            } catch {
                print(error)
            }
        }
    }
    
    func handleLoginProcess(withEmail email: String, password: String) {
        Task {
            do {
                try await self.signIn(withEmail: email, password: password)
            } catch {
                alertItem = ValidationError.handleError(error as! ValidationError)
            }
        }
    }
    
    func signIn(withEmail email: String, password: String) async throws {
        do {
            let result = try await authManager.signIn(withEmail: email, password: password)
            self.userSession = result.user
             fetchUser()
        } catch {
            throw ValidationError.userNotFound
        }
    }
    
    func handleRegistration(withEmail email: String, password: String, fullname: String) {
        Task {
            do {
                try await createUser(withEmail: email, password: password, fullname: fullname)
            } catch {
                self.alertItem = ValidationError.handleError(error as! ValidationError)
            }
        }
    }
    
    func createUser(withEmail email: String, password: String, fullname: String) async throws {
        do {
            let result = try await authManager.register(withEmail: email, password: password, fullname: fullname)
            self.userSession = result.user
            
            try await authManager.uploadUserToDatabase(result: result, email: email, fullname: fullname)
             fetchUser()
        } catch {
            throw ValidationError.userAlreadyExist
        }
    }
    
    func addToFavourites(id:Int) async throws {
        currentUser?.favouriteProgramsIDS.append(id)
        try? await updateWatchList()
    }
    
    func removeFromFavourites(id:Int) async throws {
        currentUser?.favouriteProgramsIDS.remove(at: currentUser?.favouriteProgramsIDS.firstIndex(of: id) ?? 0 )
        try? await updateWatchList()
    }
    
    private func updateWatchList() async throws {
        do {
            try await Firestore.firestore().collection("users").document(currentUser!.id).updateData(["favouriteProgramsIDS" : self.currentUser?.favouriteProgramsIDS])
            
        } catch {
            
        }
    }
    
    func signOut() {
        self.alertItem = AlertItem(title: Text("Warning"), message: Text("Are you sure you want to sign out"), dismissButton: .destructive(Text("Yes"), action: {
            do {
                try self.authManager.signOut()
                self.userSession = nil
                self.currentUser = nil
            } catch {
                print("DEBUG: Failed to sign out")
            }

        }))
    }
    
    func deleteAccount() {
        
        self.alertItem = AlertItem(title: Text("Warning"), message: Text("Are you sure you want to delte your account"), dismissButton: .destructive(Text("Yes"), action: {
            
            Task {
                do {
                    try await self.authManager.deleteUser(currentUserID: self.currentUser!.id)
                    self.userSession = nil
                    self.currentUser = nil
                } catch {
                    print(error)
                }
            }
        }))
    }

    func fetchUser() {
        
        Task {
            
          
            do {
                self.currentUser = try await authManager.fetchUser()
                
                print("DEBUG: Current user is \(self.currentUser)")
            } catch {
                print(error)
            }
        }
    }
}
