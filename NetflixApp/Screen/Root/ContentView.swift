//
//  ContentView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/20/24.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var authVM: AuthenticationVM

    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            
            if authVM.userSession != nil {
                TabView {
                    Group {
                        HomeView()
                            .tabItem {
                                Image(systemName: "house")
                                Text("Home")
                            }
                        
                        NewProgramsView()
                            .tabItem {
                                Image(systemName: "movieclapper")
                                Text("New")
                            }
                        
                        AccountView()
                            .tabItem {
                                Image(systemName: "person")
                                Text("Account")
                            }
                    }
                    .toolbarBackground(Color(.secondaryDetail), for: .tabBar)
                    .toolbarBackground(.visible, for: .tabBar)
                    .background(.black)
                }
                .accentColor(.white)
            } else {
                LoginView()
            }
        }
    }
}

#Preview {
    
    ContentView()
        .environmentObject(AuthenticationVM())
}
