//
//  SearchVM.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/8/24.
//

import Foundation

@MainActor
class SearchVM: ObservableObject {
    @Published var searchResults:[Program] = []
    var programsManager: ProgramManagerProtocol
    
    init() {
        self.programsManager = ProgramManager(networkManager: NetworkManager.shared)
    }
    
    func fetchSearchResults(query: String) {
        Task {
            do {
                searchResults =  try await self.programsManager.searchPrograms(query:query)
            } catch {
                print(error)
            }
           
        }
    }
}
