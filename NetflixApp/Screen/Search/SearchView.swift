//
//  SearchView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/8/24.
//

import SwiftUI

struct SearchView: View {
    @StateObject var searchVM = SearchVM()
    @State private var searchText = ""
    private let adaptiveColumn = [
        GridItem(.adaptive(minimum: 100))
    ]
    var body: some View {
        VStack {
            ScrollView {
                LazyVGrid(columns: adaptiveColumn, spacing: 20) {
                    ForEach(searchVM.searchResults, id: \.self) { item in
                        if let imagePath = item.posterPath {
                            BasicImageView(imageUrl:imagePath)
                        }
                        
                    }
                }
                
            } .padding()
            
        } .searchable(text:  $searchText, placement: .navigationBarDrawer(displayMode: .always))
            .onChange(of: searchText, initial: false) {
                if searchText.count >= 2 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        searchVM.fetchSearchResults(query: searchText)
                    }
                }
                
            }
    }
}

#Preview {
    SearchView()
}
