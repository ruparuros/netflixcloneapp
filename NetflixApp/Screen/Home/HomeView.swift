//
//  HomeView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import SwiftUI
import Kingfisher

struct ScrollPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        defaultValue = nextValue()
    }
}

struct HomeView: View {
    @EnvironmentObject var authVM: AuthenticationVM
    @StateObject var homeVM = HomeVM()
    @State private var scrollID: Int?
    @State private var isHidden:Bool = false
    @State private var mainIMage: UIImage?
    @State var selectedMovie: Movie?
    
    @State var navigationSize: CGSize = .zero
    
    // Not implemented filtering by this navigatiion
    var navs = ["TV Shows","Movies","Programs" ]
   
    @State var isShowingMovie = true
    @State var isScrolliing: Bool = true
    @State var value: CGFloat = 0.0
    @State var initialValue = 0.0
    var body: some View {
        NavigationStack {
                ZStack(alignment: .top ) {
                    
                    if selectedMovie == nil {
                        ZStack {
                            VStack {
                                HStack {
                                    
                                    Image("Logonetflix")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 100)
                                    
                                    Spacer()
                                    
                                    NavigationLink {
                                        SearchView()
                                    } label: {
                                        Image(systemName: "magnifyingglass")
                                            .foregroundStyle(.white)
                                            .bold()
                                            .imageScale(.large)
                                    }
                                }
                                .padding(.bottom, 8)
                                .padding(.horizontal)
                                
                                if isScrolliing {
                                    ScrollView(.horizontal, showsIndicators: false) {
                                        HStack {
                                            ForEach(navs, id:\.self) {nav in
                                                Text(nav)
                                                    .foregroundStyle(.white)
                                                    .bold()
                                                    .padding(18)
                                                    .background {
                                                        Capsule()
                                                            .strokeBorder(Color.white, antialiased: true)
                                                            .background(.clear)
                                                            .frame(height: 32)
                                                    }
                                            }
                                            
                                            Spacer()
                                        }
                                        .padding(.bottom)
                                        .padding(.horizontal)
                                    }
                                    .transition(.opacity.combined(with: .move(edge: .top)).combined(with: .scale(scale: 0.7, anchor: .top)))
                                }
                            }
                            .padding(.top, 40)
                            .background(isScrolliing ?  AnyShapeStyle(.clear) : AnyShapeStyle(.ultraThinMaterial))
                            .ignoresSafeArea(.all, edges: .top)
                        }
                        .zIndex(1)
                    }
                        
                    if let user = authVM.currentUser, !homeVM.isLoading {
                        
                        LinearGradient(stops: [.init(color: homeVM.mainColor ?? .black
                                                     , location: 0.1), .init(color: .black, location: 1.0)], startPoint: .top, endPoint: .bottom)
                        .ignoresSafeArea()
                        
                        ScrollView(showsIndicators: false) {
                            GeometryReader { geometry in
                                Color.clear.frame(height:0).preference(key: ScrollPreferenceKey.self, value: geometry.frame(in: .named("scrooll")).minY)
                            }
                            
                            LazyVStack {
                                if let _ =  authVM.currentUser {
                                    if let topMovie = homeVM.trendingMovies.first {
                                        TopMoviewPreview(movie: topMovie, selectedMovie: $selectedMovie)
                                            .cornerRadius(20)
                                            .overlay(content: {
                                                RoundedRectangle(cornerRadius: 20)
                                                    .stroke(.white.opacity(0.5), lineWidth: 2)
                                            })
                                            .clipped()
                                            .padding(16)
                                            .onAppear {
                                                    homeVM.getStartColor(path:topMovie.posterPath)
                                            }
                                    }
                                }
                                
                                BasicCollectionView(title: "Trending", movies: homeVM.trendingMovies, selectedMovie: $selectedMovie, collectionType: .small)
                                    .padding(.vertical, 10)
                                
                                BasicCollectionView(title: "Popular Movies", movies: homeVM.popularMovies, selectedMovie: $selectedMovie, collectionType: .large)
                                    .padding(.vertical, 10)
                                
                                GamesCollectionView(title: "Trending Games", games: homeVM.trendingGames)
                                    .padding(.vertical, 10)
                            }
                            .padding(.top, 100)
                        }
                        .zIndex(0)
                        .onAppear {
                            initialValue = value
                            print("----------------------")
                            print(initialValue)
                            print("----------------------")
                            
                        }
                        .onPreferenceChange(ScrollPreferenceKey.self, perform: { value in
                            withAnimation(.easeOut(duration: 0.2)) {
                                if value > initialValue - 10 {
                                    isScrolliing = true
                                } else {
                                    isScrolliing = false
                                }
                            }
                            print(value)
                            self.value = value
                        })
                        .coordinateSpace(name: "scroll")
                        .overlay(alignment:.top) {
                            
                        }
                        
                        if selectedMovie != nil {
                            MovieDetailView(selectedMovie: $selectedMovie )
                                .animation(.easeIn)
                                .transition(.opacity)
                        }
                    } else {
                        HomeShimmer()
                            .padding(.top, 80)
                    }
                }
        }
        .task {
            homeVM.bindData()
        }
        
    }
}

#Preview {
    HomeView()
}
