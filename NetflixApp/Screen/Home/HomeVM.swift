//
//  HomeVM.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import Foundation
import Kingfisher
import SwiftUI

@MainActor final class HomeVM: ObservableObject {
    
    private let homeNetworkManager: MovieManagerProtocol
    private let gamesNetworkManager: GameManagerProtocol
    @Published var trendingMovies: [Movie] = []
    @Published var popularMovies: [Movie] = []
    @Published var trendingGames: [Game] = []
    @Published var mainColor: Color?
    @Published var selectedMovie: Movie?
    @Published var isLoading = false
    
    init() {
        self.homeNetworkManager = MovieManager(networkManager: NetworkManager.shared)
        self.gamesNetworkManager = GameManager(networkManager: NetworkManager.shared)
    }
    
    func getTrendingMovies() {
        Task {
            do {
                self.trendingMovies = try await homeNetworkManager.getTrendingMovies()
            } catch {
                print(error)
            }
        }
    }
    
    func getTrendingGames() {
        Task {
            do {
                self.trendingGames = try await gamesNetworkManager.getTrendingGames()
            } catch {
                print(error)
            }
        }
    }
    
    func getPopularMoviies() {
        Task {
            do {
             popularMovies = try await homeNetworkManager.getPopularMovies()
            } catch {
                print(error)
            }
        }
    }
    
    func bindData() {
        self.isLoading = true
        Task {
            do {
                self.trendingGames = try await gamesNetworkManager.getTrendingGames()
                self.trendingMovies = try await homeNetworkManager.getTrendingMovies()
                self.popularMovies = try await homeNetworkManager.getPopularMovies()
                self.isLoading = false
            } catch {
                print(error)
            }
        }
    }
    
    func getStartColor(path: String) {
        ImageDownloader.default.downloadImage(with: Api.getImageUrl(imagePath: path)!, options: [], progressBlock: nil) {
            result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self.mainColor = Color(uiColor: data.image.averageColor ?? .black)
                }
            case .failure(_):
                print()
            }
        }
    }
}
