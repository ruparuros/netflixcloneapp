//
//  HomeManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import Foundation

protocol MovieManagerProtocol {
    func getTrendingMovies() async throws -> [Movie]
    func getPopularMovies() async throws -> [Movie]
    func getMovieDataAsync(id: Int) async throws -> [Cast]
    func getTrailerVideo(id: Int) async throws -> Trailer
    func getNewPrograms() async throws -> [Movie]
}

final class MovieManager: MovieManagerProtocol {
        
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getTrendingMovies() async throws -> [Movie] {
        let popularMoview: MovieRequest =  try await networkManager.getDataAsync(url: Api.TRENDING_MOVIES)
        return popularMoview.results
    }
    
    func getPopularMovies() async throws -> [Movie] {
        let popularMoview: MovieRequest =  try await networkManager.getDataAsync(url: Api.MOVIE_POPULAR)
        return popularMoview.results
    }
    
    func getMovieDataAsync(id: Int) async throws -> [Cast] {
        guard let url = URL(string: Api.getCastApi(id: id)) else {
            throw ApiError.invalidURL
        }

        let request: MovieCastRequest =  try await networkManager.getDataAsync(url: Api.getCastApi(id: id))
        return request.cast
    }
    
    func getTrailerVideo(id: Int) async throws -> Trailer {
        let request: TrailerRequest =  try await networkManager.getDataAsync(url: Api.getTrailerApi(id: id))
        return request.trailer
    }
    
    func getNewPrograms() async throws -> [Movie] {
        let newPrograms :NewProgramsRequest = try await self.networkManager.getDataAsync(url: Api.NEW_PROGRAMS)
        return newPrograms.results
    }
}
