//
//  MovieDetailManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/7/24.
//

import Foundation

protocol MovieDetailManagerProtocol {
    func getCast(id: Int, handler:@escaping (Result<MovieCastRequest, ApiError>) -> Void )
    func getMovieDataAsync(id: Int) async throws -> [Cast]
    func getTrailerVideo(id: Int) async throws -> Trailer
}

struct MovieDetailManager: MovieDetailManagerProtocol {
   
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getCast(id: Int, handler:@escaping (Result<MovieCastRequest, ApiError>) -> Void ) {
        networkManager.getData(url: Api.getCastApi(id: id), completed: handler)
    }

    func getMovieDataAsync(id: Int) async throws -> [Cast] {
        guard let url = URL(string: Api.getCastApi(id: id)) else {
            throw ApiError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        do {
            return  try JSONDecoder().decode(MovieCastRequest.self, from: data).cast
        } catch {
            throw ApiError.invalidData
        }
    }
    
    func getTrailerVideo(id: Int) async throws -> Trailer {
        
        let request: TrailerRequest =  try await networkManager.getDataAsync(url: Api.getTrailerApi(id: id))
            
        return request.trailer 
    }
    
}
