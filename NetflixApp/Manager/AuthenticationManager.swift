//
//  AuthenticationManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 4/16/24.
//

import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth
import FirebaseFirestoreSwift

protocol AuthenticationManagerProtocol {
    func getCurrentUser() -> FirebaseAuth.User?
    func signIn(withEmail email: String, password: String) async throws -> AuthDataResult
    func register(withEmail email: String, password: String, fullname: String) async throws -> AuthDataResult
    func uploadUserToDatabase(result: AuthDataResult, email: String, fullname: String) async throws
    func fetchUser() async throws ->  User?
    func signOut() throws
    func deleteUser(currentUserID: String)  async throws
}

struct AuthenticationManager: AuthenticationManagerProtocol {
   
    
    
    func getCurrentUser() -> FirebaseAuth.User? {
        return Auth.auth().currentUser
    }
    
    func signIn(withEmail email: String, password: String) async throws -> AuthDataResult {
        return try await Auth.auth().signIn(withEmail: email, password: password)

    }
    
    func register(withEmail email: String, password: String, fullname: String) async throws -> AuthDataResult {
        return try await Auth.auth().createUser(withEmail: email, password: password)
     
    }
    
    func uploadUserToDatabase(result: AuthDataResult, email: String, fullname: String) async throws {
        let user = User(id: result.user.uid, fullname: fullname, email: email)
        let encodedUser = try Firestore.Encoder().encode(user)
        
        try await Firestore.firestore().collection("users").document(user.id).setData(encodedUser)
    }
    
    func fetchUser() async throws ->  User? {
        guard let uid = Auth.auth().currentUser?.uid else { return nil }
        
        guard let snapshot = try? await Firestore.firestore().collection("users").document(uid).getDocument() else { return nil }
        
        return try? snapshot.data(as: User.self)
    }
    
    func deleteUser(currentUserID: String) async  throws {
        let user = getCurrentUser()
            try await user?.delete()
            try await Firestore.firestore().collection("users").document(currentUserID).delete()
            try self.signOut()
        
    }
    
func signOut() throws {
        try Auth.auth().signOut()
    }
    
    
}
