//
//  NewProgramsManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/14/24.
//

import Foundation

protocol NewProgramsManagerProtocol {
    func getNewPrograms() async throws -> [Movie]
}

struct NewProgramsManager: NewProgramsManagerProtocol {
    
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getNewPrograms() async throws -> [Movie] {
        let newPrograms :NewProgramsRequest = try await self.networkManager.getDataAsync(url: Api.NEW_PROGRAMS)
        return newPrograms.results
    }
}
