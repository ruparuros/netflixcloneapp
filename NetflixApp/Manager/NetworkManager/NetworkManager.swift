//
//  NetworkManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import Foundation

final class NetworkManager {
    static let shared =  NetworkManager()
    private init() {}
//    func getData<T: Decodable>(url: String, completed: @escaping (Result<T, ApiError>) -> Void) {
//        guard let url = URL(string: url) else {
//            completed(.failure(.invalidURL))
//            return
//        }
//        
//        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
//            
//            if let _ = error {
//                completed(.failure(.unabletToComplite))
//                return
//            }
//            
//            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
//                completed(.failure(.invalidResponse))
//                return
//            }
//            
//            guard let data = data else {
//                completed(.failure(.invalidResponse))
//                return
//            }
//            
//            do {
//                let decoder = JSONDecoder()
//                let decodedResponse = try decoder.decode(T.self, from: data)
//                completed(.success(decodedResponse))
//                
//            } catch {
//                completed(.failure(.invalidData))
//            }
//        }
//        task.resume()
//    }
    
    func getArrayDataAsync<T: Decodable>(url: String) async throws -> [T] {
        guard let url = URL(string: url) else {
            throw ApiError.invalidURL
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        do {
            return  try JSONDecoder().decode([T].self, from: data)
        } catch {
            throw ApiError.invalidData
        }
    }
    
    func getDataAsync<T: Decodable>(url: String) async throws -> T {
        guard let url = URL(string: url) else {
            throw ApiError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        do {
            return  try JSONDecoder().decode(T.self, from: data)
        } catch {
            throw ApiError.invalidData
        }
    }
}
