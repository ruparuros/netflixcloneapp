//
//  FavouritesManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/7/24.
//

import Foundation

protocol ProgramManagerProtocol {
    func  getProgramByID(id: Int) async throws -> MovieDetail
    func searchPrograms(query: String) async throws -> [Program]
}

struct ProgramManager: ProgramManagerProtocol {
    
    private var networkManager: NetworkManager

    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }

    func getProgramByID(id: Int) async throws -> MovieDetail {
        let movie :MovieDetail = try await self.networkManager.getDataAsync(url: Api.getProgramByIdeApi(id: id))
       
        return movie
    }
    
    func searchPrograms(query: String) async throws -> [Program] {
        print(Api.getSearchApi(query: query))
        let programRequest:ProgramRequest = try await self.networkManager.getDataAsync(url: Api.getSearchApi(query: query))
        return programRequest.results
    }
}
