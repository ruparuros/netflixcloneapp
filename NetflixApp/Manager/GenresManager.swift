//
//  GenresManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import Foundation

protocol GenresManagerProtocol {
    func getListOfTV_Genres() async throws -> [Genre]
    func getListOfMoviee_Genres() async throws -> [Genre]
    func getGenres(genreType: MediaType, ids: [Int]) async throws -> [Genre]
}

final class GenresManager: GenresManagerProtocol {
    
    func getGenres(genreType: MediaType, ids: [Int]) async throws -> [Genre] {
        
        switch genreType {
        case .movie:
            let genres = try await getListOfMoviee_Genres()
            
            return genres.filter { genre in
                if ids.contains(genre.id) {
                    return true
                } else {
                    return false
                }
            }
        case .tv:
            let genres = try await getListOfTV_Genres()
            
            return genres.filter { genre in
                if ids.contains(genre.id) {
                    return true
                } else {
                    return false
                }
            }
        }
    }
    
    private var networkManager: NetworkManager
    
    init() {
        self.networkManager = NetworkManager.shared
    }
    
    func getListOfTV_Genres() async throws -> [Genre] {
        do {
        let genreRequest: GenresRequest = try await networkManager.getDataAsync(url: Api.TV_LIST_GENRES)

        return genreRequest.genres
        } catch {
            print(error)
           return []
        }
    }
    
    func getListOfMoviee_Genres() async throws -> [Genre] {
        
        do {
            let genreRequest: GenresRequest = try await networkManager.getDataAsync(url: Api.MOVIE_LIST_GENRES)
            return genreRequest.genres
        } catch {
            print(error)
           return []
        }
    }
    

}
