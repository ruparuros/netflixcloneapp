//
//  GameManager.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/25/24.
//

import Foundation

protocol GameManagerProtocol {
    func getTrendingGames() async throws -> [Game]
}

struct GameManager: GameManagerProtocol {
    
    private var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getTrendingGames() async throws -> [Game] {
        return try await self.networkManager.getDataAsync(url: Api.API_GAMES)
    }
}
