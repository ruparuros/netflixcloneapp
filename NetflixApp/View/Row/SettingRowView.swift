//
//  SettingRowView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/19/24.
//

import SwiftUI

struct SettingRowView: View {
    var imageName: String
    var title: String
    var tintColor: Color
    var body: some View {
        HStack(spacing: 12) {
            Image(systemName: imageName)
                .imageScale(.small)
                .font(.title)
                .foregroundColor(tintColor)
            
            Text(title)
                .font(.subheadline)
                .foregroundStyle(.black)
        }
    }
}

#Preview {
    SettingRowView(imageName: "house", title: "home", tintColor: Color(.systemGray))
}
