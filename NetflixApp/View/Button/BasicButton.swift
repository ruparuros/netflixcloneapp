//
//  BasicButton.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import SwiftUI

struct BasicButton: View {
@State var title: String
   @Binding var imageName: String
    var color: Color
    var action: () -> Void
    
    var body: some View {
        
        Button {
          action()
        } label: {
            
            Label(
                title: { Text(title)
                        .font(.system(size: 15))
                        .fontWeight(.bold)
                    },
                icon: { Image(systemName: imageName)
                }
            )
            
            .frame(maxWidth: .infinity)
            .frame(height: 50)
            .padding(4)
            .background(color)
        }
       
        .contentTransition(.symbolEffect(.replace))
    }
}

#Preview {
    BasicButton(title: "32", imageName: .constant("play.fill"), color: Color(.red), action: {})
}
