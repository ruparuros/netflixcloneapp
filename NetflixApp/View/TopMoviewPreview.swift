//
//  TopMoviewPreview.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import SwiftUI
import Kingfisher

struct TopMoviewPreview: View {
    @EnvironmentObject var authVM: AuthenticationVM
    var movie: Movie
    var mainGenre: String?
    let genresManager = GenresManager()
    @State var genres:[Genre] = []
    @Binding var selectedMovie: Movie?
    @State var isAddedToList = false

    var body: some View {
        ZStack {
            KFImage.url(Api.getImageUrl(imagePath: movie.posterPath))
                .placeholder { _ in
                    Color(.black)
                }
                .resizable()
                .scaledToFill()
                .clipped()

            VStack {
                Spacer()
                
                HStack {
                    Spacer()
                    ForEach(Array(genres.enumerated().prefix(3)), id: \.offset) { index, element in
                        Text(element.name)
                            .font(.system(size: 16))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 1)
                        if index != 2 {
                            Circle()
                                .fill()
                                .frame(width: 5, height: 5)
                        }
                    }
                    Spacer()
                }
                .foregroundStyle(.white)
                .padding(.bottom, 10)
                
                HStack( spacing: 10) {
                    BasicButton(title: "Play", imageName: .constant("play.fill"), color: .white) {
                        selectedMovie = movie
                    }
                    .foregroundStyle(.black)
                    .cornerRadius(10)
                    .padding()
                    
                    Button {
                        Task {
                            if isAddedToList {
                                do {
                                    try  await authVM.removeFromFavourites(id: movie.id)
                                } catch {
                                    print(error)
                                }
                            } else {
                                do {
                                    try  await authVM.addToFavourites(id: movie.id)
                                } catch {
                                    print(error)
                                }
                            }
                            isAddedToList.toggle()
                            
                        }
                    } label: {
                        Label(
                            title: { Text("My List")
                                    .font(.system(size: 15))
                                    .fontWeight(.bold)
                                },
                            icon: { Image(systemName: isAddedToList ? "checkmark": "plus")
                            }
                            
                        )
                        .frame(maxWidth: .infinity)
                        .frame(height: 50)
                        .padding(4)
                        .background(Color(.secondaryDetail))
                    }
                    .foregroundStyle(.white)
                    .cornerRadius(10)
                    .padding()
                    .contentTransition(.symbolEffect(.replace))
                    
                }
                .frame(maxWidth: 400)
                .frame(height: 60)
                .padding(.bottom,20)
            }
        }.onAppear {
            if let user = authVM.currentUser {
                isAddedToList =  user.favouriteProgramsIDS.contains(movie.id)
            }
           
            Task {
              
            }
            print(isAddedToList)
        }
        .task {
            do {
                self.genres = try await self.genresManager.getGenres(genreType: movie.mediaType ??  .movie, ids: movie.genreIDS)
            } catch {
                print(error)
            }
        }
    }
}
