//
//  GamesCollectionView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/25/24.
//

import SwiftUI
import Kingfisher

struct GamesCollectionView: View {
    var title: String
    var games: [Game]
    
    var body: some View {
        
        VStack {
            HStack{
                Text(title)
                    .font(.title2)
                    .foregroundStyle(.white)
                    .fontWeight(.bold)
                   
                Spacer()
                
            }
            .padding(.horizontal,16)

            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 8) {

                   ForEach(games) { game in
                       BasicImageViewGame(imageUrl: game.thumbnail)
                           
                        }
                }
                .padding(.horizontal,16)
            }
        }
       
    }
}

struct BasicImageViewGame: View {
    
    var imageUrl: String
    
    var body: some View {
        KFImage.url(URL(string: imageUrl))
            .placeholder { _ in
                Color(.white)
            }
            .resizable()
            .frame(width: 120, height: 120)
            .scaledToFill()
            .cornerRadius(10)
                           
    }
}

#Preview {
    GamesCollectionView(title: "Games", games: [])
}
