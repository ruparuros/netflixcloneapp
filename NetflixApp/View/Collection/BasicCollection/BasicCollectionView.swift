//
//  BasicCollectionView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import SwiftUI

struct BasicCollectionView: View {
    
    var title: String
    var movies: [Movie]
    @Binding var selectedMovie: Movie?
    var collectionType: CollectionType
    var body: some View {
        
        VStack {
            HStack {
                Text(title)
                    .font(.title2)
                    .foregroundStyle(.white)
                    .fontWeight(.bold)
                   
                Spacer()
                
            }
            .padding(.horizontal,16)
            
            ScrollView(.horizontal, showsIndicators: false) {
               HStack(spacing: 16) {
                    ForEach(movies) { movie in
                        switch collectionType {
                        case .small:
                            BasicImageView(imageUrl: movie.posterPath)
                                .onTapGesture {
                                    selectedMovie = movie
                                }
                        case .large:
                            LargeImageView(imageUrl: movie.posterPath)
                                .onTapGesture {
                                    selectedMovie = movie
                                }
                        }
                    }
                   
                }
                .padding(.horizontal,16)
            }
        }
        
    }
}

enum CollectionType {
    case small
    case large
}

#Preview {
    ZStack {
        Color(.black)
        BasicCollectionView(title: "Collection", movies: MovieMockData.movies, selectedMovie:  .constant(MovieMockData.exampleMovie1), collectionType: .large)
    }
}
