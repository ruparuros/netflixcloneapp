//
//  NewProgramsShimmer.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/15/24.
//

import SwiftUI

struct NewProgramsShimmer: View {
    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            VStack {
                ScrollView {
                    VStack {
                        ForEach(1..<5) { _ in
                            ShimmerCell()
                                .frame(maxWidth: .infinity, minHeight: 300)
                                .clipShape(RoundedRectangle(cornerRadius: 10))
                                .padding()
                        }
                    }
                }
            }
        }
    }
}

#Preview {
    NewProgramsShimmer()
}
