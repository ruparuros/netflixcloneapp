//
//  ShimmerCell.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/15/24.
//

import SwiftUI

struct ShimmerCell: View {

    @State private var isAnimating = false
    @State var startPoint = UnitPoint(x:-1.8, y:-1.2)
    @State var endPoint = UnitPoint(x:0, y:-0.2)
    
    private var gradientColors = [
        Color(uiColor: UIColor.systemGray5),
        Color(uiColor: UIColor.systemGray6),
        Color(uiColor: UIColor.systemGray5)
    ]
    
    var body: some View {
        LinearGradient(colors: gradientColors, startPoint: startPoint , endPoint: endPoint)
            .onAppear {
                withAnimation(.easeIn(duration: 1).repeatForever(autoreverses: false)) {
                    startPoint = .init(x: 1, y: 1)
                    startPoint = .init(x: 2.2, y: 2.2)
                }
            }
    }
}

#Preview {
    ShimmerCell()
}
