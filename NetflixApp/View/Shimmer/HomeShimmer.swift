//
//  HomeShimmer.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/15/24.
//

import SwiftUI

struct HomeShimmer: View {
    var body: some View {
        ZStack {
            Color(.secondaryDetail)
                .ignoresSafeArea()
            
            VStack {
                ShimmerCell()
                    .frame(maxWidth:.infinity, maxHeight: 400)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .padding()
                ScrollView(.horizontal) {
                    HStack {
                        ForEach(1..<4) {_ in
                            ShimmerCell()
                                .frame(width: 100, height: 189)
                                .clipShape(RoundedRectangle(cornerRadius: 10))
                                .padding(.trailing,10)
                        }
                    }
                    .padding()
                }.scrollDisabled(true)
            }
        }
    }
}

#Preview {
    HomeShimmer()
}
