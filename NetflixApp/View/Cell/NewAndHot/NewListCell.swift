//
//  NewListCell.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/13/24.
//

import SwiftUI
import Kingfisher

struct NewListCell: View {
    
   @State var movie:Movie
    
    var body: some View {
        
        ZStack {
            Color.black
            VStack(alignment: .leading) {
                KFImage.url(Api.getImageUrl(imagePath: movie.backdropPath ))
                        .placeholder { _ in
                            Color(.black)
                        }
                        .resizable()
                        .scaledToFill()
                        .containerRelativeFrame(.horizontal) { size, _ in
                                size
                            }
                        .frame(height: 200)
                        .clipped()
                        .padding(.horizontal,16)
                     
                VStack(alignment: .leading) {
                    HStack {
                        
                        Image("Logonetflix")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 50)
                            .clipped()
                        
                        Button {} label: {
                            ReactionButtonStyleView(title: "Remind me", systemImageName: "bell")
                        }
                        
                        Button {} label: {
                            ReactionButtonStyleView(title: "Info", systemImageName: "info.circle")
                        }
                        
                    }
                    Text(movie.title)
                        .font(.title)
                        .fontWeight(.medium)
                        .foregroundStyle(.white)
                        .padding(.vertical,5)
                    
                    Text(movie.overview)
                        .foregroundStyle(.white)
                        .font(.system(size: 15))
                }
                .padding(.horizontal,16)
                    
            }
            
        }     
    }
}

#Preview {
    ZStack {
        Color.black
        NewListCell(movie: MovieMockData.exampleMovie1)
    }
   
}
