//
//  FavouritesViewCell.swift
//  NetflixApp
//
//  Created by Uros Rupar on 3/11/24.
//

import SwiftUI
import Kingfisher

struct FavouritesViewCell: View {
    
    var program: MovieDetail
    
    @State var isAddedToList: Bool = true
    @EnvironmentObject var authVM: AuthenticationVM
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            KFImage.url(Api.getImageUrl(imagePath: program.posterPath))
                .placeholder { _ in
                    Color(.white)
                }
                .resizable()
                .frame(width: 120, height: 160)
                .scaledToFill()
                .clipShape(RoundedRectangle(cornerRadius: 10))
                        
                Button {
                    Task {
                        if isAddedToList {
                            do {
                                try  await authVM.removeFromFavourites(id: program.id)
                            } catch {
                                print(error)
                            }
                        } else {
                            do {
                                try  await authVM.addToFavourites(id: program.id)
                            } catch {
                                print(error)
                            }
                        }
                        isAddedToList.toggle()
                        
                    }
                } label: {
                    Image(systemName:isAddedToList ? "heart.fill" : "heart")
                        .padding(20)
                        .background( Color.black.opacity(0.5))
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                }
        }
        .onAppear {
            isAddedToList = authVM.currentUser!.favouriteProgramsIDS.contains(program.id)
        }
    }
}
