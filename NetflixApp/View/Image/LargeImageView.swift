//
//  LargeImageView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/8/24.
//

import SwiftUI
import Kingfisher

struct LargeImageView: View {
    
    var imageUrl: String
    
    var body: some View {
        KFImage.url(Api.getImageUrl(imagePath: imageUrl))
            .placeholder { _ in
                Color(.white)
            }
            .resizable()
            .frame(width: 170, height: 290)
            .scaledToFill()
            .cornerRadius(10)
                           
    }
}

#Preview {
    BasicImageView(imageUrl: "https://image.tmdb.org/t/p/w500/1E5baAaEse26fej7uHcjOgEE2t2.jpg")
}
