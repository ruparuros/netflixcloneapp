//
//  MainImageView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/12/24.
//

import SwiftUI
import Kingfisher

struct BasicImageView: View {
    
    var imageUrl: String
    
    var body: some View {
        KFImage.url(Api.getImageUrl(imagePath: imageUrl))
            .placeholder { _ in
                Color(.white)
            }
            .resizable()
            .frame(width: 120, height: 160)
            .scaledToFill()
            .cornerRadius(10)
                           
    }
}

#Preview {
    BasicImageView(imageUrl: "https://image.tmdb.org/t/p/w500/1E5baAaEse26fej7uHcjOgEE2t2.jpg")
}
