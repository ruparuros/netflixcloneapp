//
//  NetflixAppApp.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/12/24.
//

import SwiftUI
import Firebase

@main
struct NetflixAppApp: App {
    @StateObject var authVM = AuthenticationVM()

    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            
           ContentView()
                .environmentObject(authVM)
        }
    }
}
