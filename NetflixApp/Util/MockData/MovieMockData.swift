//
//  MovieMockData.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import Foundation

struct MovieMockData {
    static let exampleMovie1 = Movie(adult: false, backdropPath: "/f1AQhx6ZfGhPZFTVKgxG91PhEYc.jpg", id: 753342, title: "Napoleon", originalLanguage: "en", originalTitle: "NApoleon", overview: "An epic that details the checkered rise and fall of French Emperor Napoleon Bonaparte and his relentless journey to power through the prism of his addictive, volatile relationship with his wife, Josephine.", posterPath: "/jE5o7y9K6pZtWNNMEw3IdpHuncR.jpg", mediaType: .movie, genreIDS: [36, 10752, 18], popularity: 9.6, releaseDate:"2023-11-22", firstAirDate: nil, video: false, voteAverage: 6.6465, voteCount: 1125)
    static let exampleMovie2 =  exampleMovie1
    static let movies = [exampleMovie1, exampleMovie2, exampleMovie1, exampleMovie2]
}
