//
//  GenreType.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/15/24.
//

import Foundation

enum GenreType {
    case tv
    case move
}
