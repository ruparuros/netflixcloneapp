//
//  ApiError.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import Foundation
import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
}

enum ApiError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case unabletToComplite
    
    var alertTitle:String {
        switch self {
        
        case .invalidURL:
            return "Invalid url"
        case .invalidResponse:
            return "Invalid response"
        case .invalidData:
            return "Invalid data"
        case .unabletToComplite:
            return "unable to complite"
        }
    }
    
    func handleError(_ error:ApiError) -> AlertItem {
        return AlertItem(title: Text(error.alertTitle), message: Text(error.localizedDescription), dismissButton: .default(Text("Ok")))
    }

}

extension ApiError:LocalizedError {
    var errorDescription: String? {
        
        switch self {
        case .invalidURL:
            return  "Url is not valid"
        case .invalidResponse:
            return "Response is not valid"
        case .invalidData:
            return "Data is not valid"
        case .unabletToComplite:
            return "Call is unable to complite"
        }
    }
    
}

enum ValidationError:Error {
    
    case userAlreadyExist
    case passworsDontMatch
    case passwordIsShort
    case usernameOrPasswordFails
    case emptyField
    case userNotFound
    
    var alertTitle:String {
        switch self {
        
        case .userAlreadyExist:
            return "User with this Username already exist"
        case .passworsDontMatch:
            return "passwords dont match"
        case .usernameOrPasswordFails:
            return "Username or Password fail"
        case .passwordIsShort:
            return "Invalid Password"
        case .emptyField:
            return "All fields are requred"
        case .userNotFound:
            return "User is not found"
        }
    }
    
   static func handleError(_ error:ValidationError) -> AlertItem {
        return AlertItem(title: Text(error.alertTitle), message: Text(error.localizedDescription), dismissButton: .default(Text("Ok")))
    }
}

extension ValidationError:LocalizedError {
    var errorDescription: String? {
        
        switch self {
        case .userAlreadyExist:
            return  "User with this username already exist"
        case .passworsDontMatch:
            return "passwords dont match"
        case .usernameOrPasswordFails:
            return "Incorrect password"
        case .emptyField:
            return "All fields are requred"
        case .userNotFound:
            return "Email or password are not correct"
        case .passwordIsShort:
            return "Paasword requires minimum 3 characters"
        }
    }
}
