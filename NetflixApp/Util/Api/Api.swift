//
//  Api.swift
//  NetflixApp
//
//  Created by Uros Rupar on 1/13/24.
//

import Foundation

struct Api {
    static let BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original/"
    static let API_KEY = "?api_key=8054fbd1fe299bd85e3ea6400f6649d7"
    static let TRENDING_MOVIES = "https://api.themoviedb.org/3/trending/movie/day\(API_KEY)"
    static let TV_LIST_GENRES = "https://api.themoviedb.org/3/genre/tv/list\(API_KEY)"
    static let API_GAMES = "https://www.freetogame.com/api/games"
    static let MOVIE_LIST_GENRES = "https://api.themoviedb.org/3/genre/movie/list\(API_KEY)"
    static let MOVIE_POPULAR = "https://api.themoviedb.org/3/movie/popular\(API_KEY)"
    static let NEW_PROGRAMS = "https://api.themoviedb.org/3/movie/upcoming\(API_KEY)"
    
    static func getImageUrl(imagePath: String) -> URL? {
        return URL(string:BASE_IMAGE_URL + imagePath )
    }
    
    static func getCastApi(id: Int) -> String {
        return"https://api.themoviedb.org/3/movie/\(id)/credits\(API_KEY)"
        
    }
    
    static func getTrailerApi(id: Int) -> String {
        return "https://api.kinocheck.de/movies?tmdb_id=\(id)"
    }
    
    static func getProgramByIdeApi(id: Int) -> String {
        return "https://api.themoviedb.org/3/movie/\(id)\(API_KEY)"
    }
    
    static func getSearchApi(query: String) -> String {
        return "https://api.themoviedb.org/3/search/multi?query=\(query)&api_key=8054fbd1fe299bd85e3ea6400f6649d7"
    }
}
