//
//  ActivityView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/7/24.
//

import Foundation

import SwiftUI

struct ActivityIndicator: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        let activityIndicatorViiew = UIActivityIndicatorView(style: .large)
        activityIndicatorViiew.color = .white
        activityIndicatorViiew.startAnimating()
        return activityIndicatorViiew
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
    }
}

struct LoadingView: View {
    var body: some View {
        ZStack {
            Color(.black)
                .edgesIgnoringSafeArea(.all)
            ActivityIndicator()
        }
    }
}
