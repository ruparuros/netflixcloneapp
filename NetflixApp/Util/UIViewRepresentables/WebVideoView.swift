//
//  WebVideoView.swift
//  NetflixApp
//
//  Created by Uros Rupar on 2/7/24.
//

import Foundation
import WebKit
import UIKit
import SwiftUI

struct WebView: UIViewRepresentable {
 
    var url: URL
 
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
 
    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}

struct WebVideoView: View {
    let url: URL
    var body: some View {
       
            WebView(url: url)
        
    }
}
